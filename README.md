# The project

The project is: ___RT Key Managment___. You have to manage the keys of the R&T department.
This means that :

- At anytime, it should be possible to localize the keys. (We assume that they stay within the IUT campus)
- If a key battery is to low, it should raise an alert. (You choose the alert)
- We should be able to know who borrowed the keys (use your imagination, implements some mechanisms).
- Keys are also a way to monitor the RT departement with other sensors (cooling system, ...) 
- Key states (localisation, sensor states, ...) should be easily accessible.

For this, we have at our disposition: a LinkItOne with multiples modules.
The condition: we must pursue the project of someone else. It's up to us to choose if we modify, or continue without any rectification, or restart from nothing. 

## Quick start
#### Installation
You can follow a tutorial just [here](https://gitlab.com/m4207/td/blob/master/td1.md)

At first, I tried to install on Linux but I had some problems. So after hours of pain, I abandoned and switched for windows. It's really easier!!

#### Unitary tests
1. __Blinking :__ The first test to do is to make blink the LED   =>   [code](https://gitlab.com/anaismn/m4207/blob/master/test/blinking.ino)
2. __Hello World! :__ It's time for the famous HelloWorld! The mission: to print "Hello World"  =>  [code](https://gitlab.com/anaismn/m4207/blob/master/test/hello_world.ino)
3. __Counter :__ Now our objective is to  write a code that create a counter and print the following line (loop) on
the serial output: `the value of the counter is x`, where `x` increments by `1` every seconds.  =>  [code](https://gitlab.com/anaismn/m4207/blob/master/test/counter.ino)  

    The result must be : 1  
                         2  
                         3...
 
4. __GPS :__We are going to test the GPS. This code will prints the output of the GPS on the serial link.   =>   [code](https://gitlab.com/anaismn/m4207/blob/master/test/gps.ino)